// Generate menu in HTML
const filterTocIgnore = el => !el.parentElement.classList.contains('toc-ignore')
const allHeadings = Array.from(document.querySelector('.rendered-content').querySelectorAll('h1, h2, h3')).filter(filterTocIgnore)
const menuDiv = document.querySelector('#tocMenu')
let lastHeadingNo = 0
let menuHTML = ''
const h1Headings = allHeadings.filter(el => el.tagName === 'H1')
const removeFirstHeading = h1Headings.length === 1 && allHeadings[0].tagName === 'H1'
if (removeFirstHeading) {
    allHeadings.shift()
    lastHeadingNo = 1
}
allHeadings.forEach(h => {
    let currentHeadingNo = parseInt(h.tagName.slice(1))
    while (currentHeadingNo !== lastHeadingNo) {
        if (currentHeadingNo > lastHeadingNo) {
            menuHTML += '<ul>'
            lastHeadingNo++
        } else {
            menuHTML += '</ul>'
            lastHeadingNo--
        }
    }

    lastHeadingNo = currentHeadingNo
    menuHTML += `<li>${h.innerHTML}</li>`
})
menuDiv.innerHTML = menuHTML

// Use an array to cache heading locations
let headingsTop = []
setTimeout(updateHeadingOffsets, 1)        // Headings move right after first render
window.onresize = updateHeadingOffsets     // Headings move when resizing
updateHeadingOffsets()                     // We need the values in next section
function updateHeadingOffsets() {
    headingsTop = []
    allHeadings.forEach(h => headingsTop.push(h.offsetTop))
}
if (removeFirstHeading) {
    headingsTop.shift()
}

// Update on scroll
window.onscroll = function() {
    const scrollPosition = (document.documentElement.scrollTop || document.body.scrollTop) + 0
    const activeElements = document.querySelectorAll('#tocMenu .active')
    activeElements.forEach(item => item.classList.remove('active'))

    for (var i = 0; i < headingsTop.length; i++) {
        if (scrollPosition > headingsTop[i] && (!headingsTop[i+1] || scrollPosition < headingsTop[i+1])) {
            const listItem = document.querySelectorAll(`#tocMenu li`)[i]
            listItem.classList.add('active')
            activateParent(listItem)
        }
    }
}
function activateParent(child) {
    const parent = child.parentElement
    if (parent) {
        parent.classList.add('active')
        activateParent(parent)
    }
}

// Scroll on click on a menu item
const menuElements = Array.from(document.querySelectorAll('#tocMenu li'))
const headings = Array.from(document.querySelectorAll(`.rendered-content h1, .rendered-content h2, .rendered-content h3`)).filter(filterTocIgnore)
if (removeFirstHeading) {
    headings.shift()
}
menuElements.forEach((h, elIndex) => {
    h.addEventListener('click', e => {
        headings[elIndex].scrollIntoView()
    })
})
