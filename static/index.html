<!DOCTYPE html>

<head>
    <meta charset="UTF-8">
    <base target="_blank">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Bashboard</title>

    <link rel="stylesheet" href="styles/default.css">
    <link rel="stylesheet" href="styles/toc.css">
</head>

<body>
    <div class="left-col">
        <h2>Bashboard</h2>
        <div id="tocMenu"></div>
    </div>
    <noscript id="markdown-content">

# One dashboard to rule them all

<div class="col toc-ignore">

### Bring in stats from all of your apps and 3rd party services into one single dashboard with zero hassle!

<br/>
<a href="https://forms.gle/LdKfGEhv7bAR7AWS8">
    <button>
        <h4>Join closed beta</h4>
    </button>
</a>

<p class="hint font_small">For a <b>demo</b> check out <a href="/bashboard/system">Bashboard's own stats</a>.
</p>

</div>
<div class="col">
    <div class="small-center">

![Illustration of data moving from multiple sources](images/dashboard.svg)
    </div>
</div>

## Every business needs metrics

You know that your business is doing well from your bank statement - that's a metric. But if that's your only metric then your business could be quite fragile. What is it that really makes profit happen? User acquisition? User return rate? System stability? Responsiveness? Measuring key points in your business shows you where you need to focus.

Yet your business isn't a single app or website. You probably have multiple apps using a dozen different 3rd party services orchestrated as your whole service. Figuring out how to get data in from all the sources is a major pain. You need an overview from _all_ of them. **Other Dashboards boast thousands of integrations, yet there's always the one you need that's missing.**

We're changing that! We don't provide inflexible fixed integrations to limited services, but very flexible API where you can just blast your data in whatever format and choose what kind of chart you want to see.

## How does it work?

Send a POST request in any format to `bashboard.io/your-user/dash-name/widget-name` from any source! It's so easy you can make up the URL on the go, no need to generate anything.

<div class="col">
    <div class="paper center-content">

```
fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
        user: user.id,
        action: 'add-product'
    })
})
```
Use post requests to send data from _anywhere_.
    </div>
</div>
<div class="col">
    <div class="paper center-content large-icon">

![Illustration of webhook](images/webhook.svg)
Use webhooks to send data from any 3rd party service.
    </div>
</div>
<div class="col">
    <div class="paper center-content">

```
curl -d "cpuLoad=96" -X POST url
```
So simple even Bash can do it!
    </div>
</div>
<div class="col">
    <div class="paper center-content">

![c++ icon](images/c++.svg) ![java icon](images/java.svg) ![javascript icon](images/javascript.svg) ![php-logo icon](images/php-logo.svg) ![python icon](images/python.svg) ![python icon](images/golang.svg)
Send stats from your backend - whatever the language!
    </div>
</div>

Don't worry about formatting your data too much - we'll figure out how to generate the dashboard from your data.

## Integrations

**None!**

**We don't provide integrations with any _specific_ services because webhooks and POST requests are supported by _all_ services!** This means the sources are virtually limitless.

Send data from anywhere: your frontend, backend, mail server, cronjob, wordpress, third party service, etc.

In any language: JavaScript, C#, Java, Bash, Python, Elixir, Haskell, etc.

Track anything: user flow, app health, email opening, IoT devices, etc.

## Open source

Our code [is open source](https://gitlab.com/bashboard/) (but it's only a prototype at the moment). So you can self-host. Our business model is a no-hassle, fast and stable hosting service - not lock in.

Currently our [forum is on Reddit](https://www.reddit.com/r/bashboard/).

## We're not for everyone

Our main selling point is that **you can get zero-to-dashboard in a single thought and pull data from multiple sources**. There's nothing to learn, no API to read, no system setup. There are situations with different priorities however.

- **Grafana**: If all you need is stats from a single database, Grafana is amazing for deep analytics.
- **Kibana**: Deep integration with your Elasticstack for data visualization and searching.
- **Zoho analytics / Plecto / Klipfolio**: If you don't know what a POST request is you're probably better off with the point-and-click solutions that may not be as flexible or have all the integrations.

It's important to choose the right dashboard for you and we know it's a hard task. So **feel free to contact us with your use case at bashboard-h@krister.ee** and we'll advise you as best as we can. We're not sales people btw. We're developers. If there's a better service out there for you - we'll tell you.

## Why no screenshots?

Just checkout [Bashboard's own stats](/bashboard/system). Not too pretty yet, we've been concentrating on the backend.

## Who's behind this?

My name is [Krister](https://krister.ee) and my team is [randomforest](https://randomforest.ee) - we build custom software for demanding clients. Contact me at bashb-gh@krister.ee.

## Why are you building this?

It is our own pain that we're solving. We've got multiple products that need statistics from different sources brought to a single dashboard - it is super hard! The only real option is to build our own private service. In fact until now we've resorted to just visiting the services and copy-pasting the stats to an excel file.. still in 2020. There are loads of Dashboard as a Service products with thousands of integrations, yet they don't quite show stats how we need them to and don't have the integrations we need. Commonly they don't even have proper webhook support. If there is an API then you have to learn their formats for hours (only to find out they still don't support some function you need). This isn't necessary!

So now we've started building our product (to be open-sourced later) and our aim is to at least cover our own needs. If there's a wider public out there, then we'd be happy to serve and build a proper product around it. So if you'd like to take a look - [sign up](https://forms.gle/LdKfGEhv7bAR7AWS8)

## Use case walkthrough

Let's say our service name is **Doggie Socks** and we want to track user flow from cold email all the way to payment.

#### Step 1. Make up a URL

The format is `https://bashboard.io/your-username/dash-name/widget-name`.

You don't first have to come to Bashboard to get the URL. Just make it up on the go and use it in your POST request.

Let's use `https://bashboard.io/kristerv/doggie/user-flow`.

#### Step 2: Configure webhook in Mailgun.

Or _any_ other email service. We can track opens, clicks, unsubscribes.

![Mailgun webhook screenshot](images/mailgun.png)


The data sent looks like this:

```
{
  "event-data": {
    "event": "opened",
    "timestamp": 1529006854.329574,
    "id": "DACSsAdVSeGpLid7TN03WA"
  }
}
```

Now any time one of our emails is opened, the data is saved in Bashboard.

#### Step 3: Track user visit on landing page in Unbounce.

Like a lot of front-end services Unbounce allows scripts. So just enter a POST request with JavaScript:

![Unbounce script screenshot](images/unbounce.png)

Notice how we're sending the data to the same endpoint on Bashboard, even though they are different data structures. Ultimately we want to display a funnel chart, so they all need to be in the same widget.

#### Step 4: Track logins in our app backend.

Just create a POST request to the same URL above using whatever language you're using there. You could also run a `curl` command from nginx or anywhere else in your server.

#### Step 5: Configure Bashboard

Navigate your browser to the dashboard with `https://bashboard.io/kristerv/doggie`. You'll see that the different data sources have been grouped automatically by source and a simple bar chart displays how many users have visited from each source (with timing filters). Of course, you can choose what chart to show and how the data is grouped.

## Tutorial

1. Make up a username. This will hold all of your dashboards.
1. Make up a dashboard name. This will hold all of your widgets. Probably the same as project name.
1. Make up a widget name. This is where you'll send the data of a single chart.
1. Make a POST request with your data to `https://bashboard.io/user/dash/widget`.
    - Below are examples (`url` is from steps 1, 2, 3 and 4)
    - Ideally with `content-type: application/json`.
    - For best results, send data recurringly. Like every minute or something.
1. Take your browser to `https://bashboard.io/user/dash/` to see the result.

Example in JavaScript

```
fetch('https://bashboard.io/user/dash/widget', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
    // data can be any object, like {user: 12345} or just a number like 67
})
```

Example in Bash

```
curl -d "cpuLoad=96" -X POST https://bashboard.io/user/dash/widget
```


    </noscript>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/markdown-it/7.0.1/markdown-it.min.js"></script>
    <script type="text/javascript">
        var mdContent = document.querySelector('#markdown-content')
        var md = window.markdownit({
            breaks: true,
            linkify: true,
            html: true,
        });
        var html = md.render(mdContent.textContent)
        var div = document.createElement('div')
        div.classList.add('rendered-content')
        div.innerHTML = html
        document.body.appendChild(div)
    </script>
    <script src="scripts/toc.js"></script>
</body>
