
import moment from "moment";

export function formatDatetimes(datetimeList) {
    const defaultFormatting = "D MMM H:mm:ss"

    if (!datetimeList) {
        return datetimeList
    }

    if (typeof datetimeList === 'string') {
        return moment(datetimeList).format(defaultFormatting)
    }

    const firstDate = moment(datetimeList[0])
    const lastDate = moment(datetimeList[datetimeList.length - 1])
    if (datetimeList.length > 1 && firstDate.isValid() && lastDate.isValid()) {
        const duration = moment.duration(lastDate.diff(firstDate))
        const durations = [
            ['YYYY', duration.as('years')],
            ['D MMM', duration.as('months')],
            ['D MMM', duration.as('days')],
            ['H:mm', duration.as('hours')],
            ['H:mm', duration.as('minutes')],
            ['H:mm:ss', duration.as('seconds')],
        ]
        let formatMatch = durations.find(([format, value]) => {
            return value >= 3 && value < 100
        })

        if (!formatMatch) {
            formatMatch = [defaultFormatting, 0]
        }

        return datetimeList.map(dt => moment(dt).format(formatMatch[0]))
    } else {
        return datetimeList.map(dt => {
            const mdt = moment(dt)
            if (mdt.isValid()) {
                return mdt.format(defaultFormatting)
            } else {
                return dt
            }
        })
    }
}

export function dropExcess(ticks, count) {
    if (count === 3) {
        return dropExcess3(ticks)
    }
    if (!ticks || ticks.length === 1) return ticks
    const skipAmount = parseInt(ticks.length / count);
    const newTicks = []
    for (let i = 0; i < ticks.length; i += skipAmount) {
        newTicks.push(ticks[i])
    }
    newTicks.push(ticks[ticks.length - 1])
    return newTicks
}

export function dropExcess3(ticks) {
    const middleIndex = parseInt(ticks.length / 2)
    return [ticks[0], ticks[middleIndex], ticks[ticks.length - 1]]
}
